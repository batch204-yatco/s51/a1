import courseData from '../data/courseData';
import CourseCard from '../components/CourseCard';


export default function Courses(){
	// Check if mock data was captured
	// console.log(courseData);
	// console.log(courseData[0]);

	// Props
		// Shorthand for "property" since components are considered as object in ReactJS
		// Way to pass data from parent component to child component
		// Synonymous to the function parameter
		// Referred to as "props drilling"
		//courseProp is user defined

	const courses = courseData.map(course => {
		return (
			<CourseCard courseProp={course} key={course.id}/>
		)
	})

	return (
		<>
			<h1>Courses</h1>
			{courses}
		</>
	)
}